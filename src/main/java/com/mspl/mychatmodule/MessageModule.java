package com.mspl.mychatmodule;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

public class MessageModule extends AppCompatActivity implements View.OnClickListener {
    private ImageView img_sync;
    private TextView toolTitle;
    private RecyclerView messageList;
    private MessageAdapter adapter;
    private ArrayList<MessageModel> arrayList;
    private ImageView sendMessage;
    private EditText editTextMessage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_module);
        initView();
    }

    private void initView() {
        editTextMessage = findViewById(R.id.editTextMessage);
        sendMessage = findViewById(R.id.sendMessage);
        img_sync = findViewById(R.id.img_sync);
        toolTitle = findViewById(R.id.toolTitle);
        messageList = findViewById(R.id.messageList);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(false);
        messageList.setLayoutManager(linearLayoutManager);


        arrayList = new ArrayList<>();
        adapter = new MessageAdapter(this, arrayList);
        messageList.setAdapter(adapter);

//        messageList.getLayoutManager().scrollToPosition(adapter.getItemCount()-1);
      /*  messageList.scrollToPosition(adapter.getItemCount()-1);
        mLinearLayoutManager.scrollToPosition(yourList.size() - 1);*/

        toolTitle.setText("MESSAGES");
        img_sync.setOnClickListener(this);
        sendMessage.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.sendMessage:

                if (editTextMessage.getText().toString().trim().length() != 0) {

                    MessageModel messageModule = new MessageModel();
                    messageModule.setReceiverName("Driver");
                    messageModule.setUserType("1");
                    messageModule.setMessage(editTextMessage.getText().toString());
                    editTextMessage.setText("");
                    arrayList.add(messageModule);
                    adapter.notifyDataSetChanged();

                } else {

                    Toast.makeText(getApplicationContext(), "No Message here", Toast.LENGTH_SHORT).show();

                }

                break;

//            case R.id.img_sync:
//                onBackPressed();
//                break;

        }

    }

}
